"use strict";

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/crypto');

var StockEntity = mongoose.Schema({
    parity: {
        type: String
    },
    exchange: String,
    interval: String,
    dataSet: [{
        open: Number,
        high: Number,
        low: Number,
        close: Number,
        date: Date,
        btcVolume: Number,
        _id: false
    }],
    _id: {
        type: String
    }
});

var StockEntityModel = mongoose.model('StockEntity', StockEntity);
module.exports = StockEntityModel;
