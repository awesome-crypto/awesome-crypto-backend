"use strict";

var StockEntityModel = require('../schema/schema.js');

class DBUtils {

    /**
        Get historical market data.
    */
    static findOne(query, callback) {
        StockEntityModel.findOne(query, 'dataSet parity exchange interval -_id', function(err, marketData) {
            if (err) {
                console.log(err);
                throw new Error(err);
            }
            return callback(err, marketData);
        });
    }
}

module.exports = DBUtils;
