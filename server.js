"use strict";

var express = require('express');
var app = express();
var DBUtils = require('./src/utils/db-utils.js')

app.get('/getmarket/exchange/:exchange/parity/:parity/interval/:interval', function(req, res) {
    DBUtils.findOne({
        exchange: req.params.exchange,
        parity: req.params.parity,
        interval: req.params.interval
    }, function(err, marketData) {
        res.send(marketData);
    });
});

app.listen(3000);
